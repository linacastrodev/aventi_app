// -----------------------------------------------------------------------------
// This file contains all logic to the screen view
// -----------------------------------------------------------------------------
async function fetchData() {
 await fetch("../data/data.json")
    .then((response) => {
      if (!response.ok) {
        throw Error("Error");
      }
      return response.json();
    })
    .then((data) => {
      const html = data
        .map((user) => {
          return `
        <div class="row" id="row">
        <div class="column col-6" id="column">
          <div class="col-lg-2">
            <div class="circle">
              <img
                src="${user.logo}"
                alt="img"
                loading="lazy"
                class="icon-img"
              />
            </div>
          </div>
          <div class="col-lg-10">
            <h6>${user.company} <label class="new">${
            user.new === true ? "New!" : user.postedAt
          }</label><label class="featured">${
            user.featured === true ? "Featured!" : "No Featured"
          }</label></h6>
            <h2>${user.position}</h2>
            <p>
              <span>${user.postedAt} </span><span> ${
            user.contract
          }</span><span> ${user.location}</span>
            </p>
          </div>
        </div>
        <div class="column col-6" id="column">
          <div class="shape">
            <p>${user.tools}</p>
          </div>
        </div>
      </div>          
          `;
        })
        .join("");
      document.querySelector("#grid").insertAdjacentHTML("afterbegin", html);
    })
    .catch((error) => {
      console.error(error);
    });
}
fetchData();