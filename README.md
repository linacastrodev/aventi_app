# Aventi Solutions TECHNICAL TEST

_This little project is about a technical test for the company **Aventi solutions** for the position:
> **Frontend Web developer.**

## Initialization 🔧

_If you have already execute the project code, you must do open the html :_

```
From the folder: /src/views/home.html
```

## Built with 🛠️

- [Javascript](https://www.javascript.com/)
- [JSON](https://www.json.org/json-en.html)
- [SCSS](https://sass-lang.com/)

## Authors ✒️

- **Lina Castro** - _Frontend and Mobile Hybrid Developer_ - [lirrums](https://gitlab.com/linacastrodev)

## License 📄

This project is under the License (MIT)

## Acknowledgements 🎁

- We greatly appreciate to the company for motivating us with the project. providing us with tools and their knowledge for our professional growth📢